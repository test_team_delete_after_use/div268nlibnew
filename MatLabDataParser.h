// MatLabDataParser.h

#ifndef _MATLABDATAPARSER_h
#define _MATLABDATAPARSER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

void MatLabParse(String Data, long int mas[], float diam[]);
//calling MatLab parser, need input string, mas of steps, which go to movement, diam to convert
//to steps, byte PM - indicator of package  mode work
#endif

