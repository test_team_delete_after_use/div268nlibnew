#pragma once
#include "Arduino.h"


class DIV268N
{
public:
	DIV268N( int count);

	void AddMotor(int numb, int Step, int Enable, int Dir, int MicroStep, int RPM);
	void Move(int Steps[]);
	void ChangeState(byte State[]);
	void CorrectData(int Count, int RPM);
};

