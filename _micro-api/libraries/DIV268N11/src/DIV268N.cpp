#include "Arduino.h"
#include "DIV268N.h"

struct sList // struct with all states that needed for driver
{
	int StepPin;
	int DirPin;
	int EnablePin;
	int MicroStep;
	unsigned long int Pulse;
	unsigned long int Time;
	byte State;
};


unsigned long int TimerMin;
//Count of drivers
int countIN = 1;
//Creating structure
sList *Motors =NULL;

//Set mode to pins(output) to control
void init(int Count)
{
	pinMode(Motors[Count].DirPin, OUTPUT);
	pinMode(Motors[Count].StepPin, OUTPUT);
	pinMode(Motors[Count].EnablePin, OUTPUT);
}

// Waiting untill timer
void microWaitUntil(unsigned long target_millis)
{
	//SLEEP_MODE_IDLE(target_millis);

	yield();
	while (millis() < target_millis);

}

//Cheking available steps
byte CheckSteps(int Steps[])
{
	for (int i = 0; i < countIN; i++)
	{
		if (Steps[i] != 0)
			return(1);
	}
	return(0);
}

//Creating structure of Motors Data, including timings and pulse time
DIV268N::DIV268N(int count)
{
	Motors=(sList*) realloc(Motors, count * sizeof(sList));
	countIN = count;
}

//Adding new Driver to structure (pins, speed and microstep Value);
void DIV268N::AddMotor(int numb, int Step, int Enable, int Dir, int MicroStep, int RPM)
{
	Motors[numb].State = 0;
	Motors[numb].Time = 0;
	Motors[numb].StepPin = Step;
	Motors[numb].EnablePin = Enable;
	Motors[numb].DirPin = Dir;
	Motors[numb].MicroStep = MicroStep;
	Motors[numb].Pulse = round(60000/( 400*Motors[numb].MicroStep * RPM));
	init(numb); //Call init function, to init pins
	Serial.println(Motors[numb].StepPin);
	Serial.println(Motors[numb].EnablePin);
	Serial.println(Motors[numb].DirPin);
}

//Changing speed of motor
void DIV268N::CorrectData(int Count,int RPM)
{
	Motors[Count].Pulse = round(60000 / (400*Motors[Count].MicroStep * RPM));
}

//move cycle
void DIV268N::Move(int Steps[])
{
	//Set direction to drivers and make all steps >0
	for (int i = 0; i < countIN; i++)
	{
		if (Steps[i] < 0)
		{
			Steps[i] = abs(Steps[i]);
			digitalWrite(Motors[i].DirPin, HIGH);
		}
		else
			digitalWrite(Motors[i].DirPin, LOW);
	}
	while (CheckSteps(Steps)==1)
	{
		unsigned long int Timer;
		for (int i = 0; i < countIN; i++)
		{
			Serial.print("Motor ");
			Serial.println(i);
			if (Steps[i] > 0) //Cycle of powering up steps
			{
				
				if (Motors[i].Time <= millis())
				{
					
					if (Motors[i].State == 0)
					{
						digitalWrite(Motors[i].StepPin, HIGH);
						Motors[i].State = 1;
						Motors[i].Time = millis() + Motors[i].Pulse;
						TimerMin = Motors[i].Time; 

					}
					else if (Motors[i].State == 1)
					{
						digitalWrite(Motors[i].StepPin, LOW);
						Motors[i].State = 2;
						Motors[i].Time = millis() + Motors[i].Pulse;
						TimerMin = Motors[i].Time;

					}
					else if (Motors[i].State == 2)
					{
						Motors[i].State = 0;
						Motors[i].Time = 0;
						Steps[i]--;
						if (Steps[i] > 0)
							Motors[i].Time = millis();
					}
				}
			}
		}
		//Founding nearest timer alarm
		for (int i = 0; i < countIN; i++)
		{
			if (Motors[i].Time != 0) {
				if (Motors[i].Time < TimerMin)
					TimerMin = Motors[i].Time;
			}
		}
		microWaitUntil(TimerMin);
	}
}

//Set state of Driver - on/off
void DIV268N::ChangeState(byte State[])
{
	for (int i = 0; i < countIN; i++)
	{
		if (State[i] = 0)
			digitalWrite(Motors[i].EnablePin, LOW);
		else
			digitalWrite(Motors[i].EnablePin, HIGH);
	}
}
