// CorrectionLibrary.h

#ifndef _CorrectionLibrary_h
#define _CorrectionLibrary_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

void MoveCorr(long int _steps[], float _diam[], int long position); //function call, correction
//Contains data if steps, diameters of bobbins, position of engine numb.8
float SpeedCorr(long int _steps[], int i); //same thing, correction speed to sync speed of engine
//numb.8 and engine of radial movement
#endif