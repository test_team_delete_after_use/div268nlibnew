// 
// 
// 
#include "Arduino.h"
#include "JSONParser.h"
#include "string.h"

String Direction(String json, long int mas[], float diam[])
{
	String array;
	int i = 0;
	while (json[i]!=']')
		i++;
	array = json.substring(1, i);
	for (int i = 0; i < 8; i++)
	{
		int j = 0;
		while (array[j] != ',' && j < array.length())
			j++;
		mas[i] = round(array.substring(0,j).toFloat()/ diam[i]);
		array = array.substring(j+1);
	}
	return(json.substring(i+1));
}

String Linear(String json, long int mas[], float diam[])
{
	int i = 0;
	while (json[i] != ',' && json[i] != '}')
		i++;
	mas[8] = round(json.substring(0, i).toFloat()/diam[8]);
	return(json.substring(i+1));
}

String System(String json, byte &PM)
{
	String system;
	int i = 1;
	while (json[i] != '"')
		i++;
	system = json.substring(1, i);
	if (system == "Pack")
	{
		PM = 1; 
	}
	if (system == "Pack/")
	{
		PM = 2;
	}
	return(json.substring(i+1));
}

byte JSONParse(String json,long int mas[],float diam[], byte &PM)
{
	json = json.substring(1);
	int caseCH = 0;
	int i = 1;
	while (json[0] != '}' && json.length()>0)
	{
		i = 0;
		if (json[0] == '"')
		{
			json=json.substring(1);
		}
		while (json[i]!='\"')
			i++;
		if (json.substring(0, i) == "Direction")
		{
			json=json.substring(i+2, json.length());
			json=Direction(json,mas,diam);
			if (json[0] == ',')
				json = json.substring(1);
		}
		else if (json.substring(0, i) == "Linear")
		{
			json=json.substring(i+2, json.length());
			json=Linear(json,mas, diam);
		}
		else if (json.substring(0, i) == "System")
		{
			json=json.substring(i+2, json.length());
			json=System(json,PM);
		}	
	}
	Serial.println("***JSON Parser done!***");
	return(PM);
}