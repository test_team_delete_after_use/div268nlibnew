
#include "MatLabDataParser.h"
//#include "Arduino.h"


void direction(String Data, long int mas[],float diam[]) //direction parser, collect length from string
{
	int i = 0;
	while (Data[i] != ']') //Search the end of the array
		i++;
	Data = Data.substring(1, i); //cut this array, end work:"1,1,1,1,1,1,1,1"
	for (int i = 0; i < 9; i++)
	{
		int j = 0;
		while (Data[j] != ',' && j < Data.length()) //Search the end of the data for motor
			j++;
		mas[i] = round(Data.substring(0, j).toFloat() / diam[i]); //add this data to movement massive, convert to steps
		Data = Data.substring(j + 1); //cut start with this data, start loop again
	}
}

void MatLabParse(String Data,long int mas[],float diam[]) //MatLab parser method
{ //Need string data, massive of movement, massive of diameters of the bobbins, signal byte Pm
		direction(Data, mas, diam); //go to direction, send all necessary inf
}