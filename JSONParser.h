// JSONParser.h
#include "string.h"
#ifndef _JSONPARSER_h
#define _JSONPARSER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

byte JSONParse(String abc, long int mas[], float diam[],byte &PacketMode);

#endif

