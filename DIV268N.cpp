#include "Arduino.h"
#include "DIV268N.h"

//#define Serial2 Serial

void DIV268N::InitPins(int8_t i) //Set mode to pins(output) to control
{
	pinMode(_motors[i].dirPin, OUTPUT);
	pinMode(_motors[i].stepPin, OUTPUT);
	pinMode(_motors[i].enablePin, OUTPUT);
}

void DIV268N::BreakMovement() //if Serial available>0, then stop movement
{
	delayMicroseconds(50); //Wait a little bit
	for (uint8_t i = 0; i < _numberOfDrivers; i++) //set motors StepsPin to low, to end full cycle
	{
		digitalWrite(_motors[i].stepPin, LOW);
	}
}

 /*
void ChangeStateMT(uint8_t a, byte b)
{
	if (b == 0)
	{
		switch (a)
		{
		case 51:
			PORTD |= B10000000;
			break;
		case 50:
			PORTD |= B10000000;
			break;
		case 44:
			PORTD |= B10000000;
			break;
		case 38:
			PORTD |= B10000000;
			break;
		case 29:
			PORTD |= B10000000;
			break;
		case 23:
			PORTD |= B10000000;
			break;
		case 4:
			PORTD |= B10000000;
			break;
		case 39:
			PORTD |= B10000000;
			break;
		case 7:
			PORTD |= B10000000;
			break;
		}
	}
	else
	{
		switch (a)
		{
		case 51:
			PORTD &= B01111111;
			break;
		case 50:
			PORTD &= B01111111;
			break;
		case 44:
			PORTD &= B01111111;
			break;
		case 38:
			PORTD &= B01111111;
			break;
		case 29:
			PORTD &= B01111111;
			break;
		case 23:
			PORTD &= B01111111;
			break;
		case 4:
			PORTD &= B01111111;
			break;
		case 39:
			PORTD &= B01111111;
			break;
		case 7:
			PORTD &= B01111111;
			break;
		}
	}
}
//*/

//*
void ChangeStateMT(uint8_t a, uint8_t b)
{
	if (b == 0)
	{
		switch (a)
		{
		case 51:
			PORTB &= B11111011;
			break;
		case 50:
			PORTB &= B11110111;
			break;
		case 44:
			PORTL &= B11010111;
			break;
		case 38:
			PORTD &= B01111111;
			break;
		case 29:
			PORTA &= B01111111;
			break;
		case 23:
			PORTA &= B11111101;
			break;
		case 4:
			PORTG &= B11011111;
			break;
		case 39:
			PORTG &= B11111011;
			break;
		case 7:
			PORTH &= B11101111;
			break;
		}
	}
	else
	{
		switch (a)
		{
		case 51:
			PORTB |= B00000100;
			break;
		case 50:
			PORTB |= B00001000;
			break;
		case 44:
			PORTL |= B00100000;
			break;
		case 38:
			PORTD |= B10000000;
			break;
		case 29:
			PORTA |= B10000000;
			break;
		case 23:
			PORTA |= B00000010;
			break;
		case 4:
			PORTG |= B00100000;
			break;
		case 39:
			PORTG |= B00000100;
			break;
		case 7:
			PORTH |= B00010000;
			break;
		}
	}
}  //*/

//Checking available steps
uint8_t DIV268N::CheckSteps(int32_t stepsFromProg[])
{
	for (uint8_t i = _numberOfDrivers-1; i>=0; i--)
	{
		if (stepsFromProg[i] >= 0)
			return(1); //if we have some steps return 1
	}
	return(0); //else 0 
}

//Creating structure of Motors Data, including timings and pulse time
DIV268N::DIV268N(uint8_t countOfDrivers)
{
	_motors = (DriverParametersList*)realloc(_motors, countOfDrivers * sizeof(DriverParametersList));
	_stepsCount = (int32_t*)realloc(_stepsCount, countOfDrivers * sizeof(int32_t));
	for (uint8_t i = 0; i < countOfDrivers; i++)
	{
		_stepsCount[i] = 0;
	}
	_numberOfDrivers = countOfDrivers;
}

//Adding new Driver to structure (pins, speed and microstep Value);
void DIV268N::AddMotor(uint8_t i, uint8_t enable, uint8_t stepPin, uint8_t dir, uint8_t microStep, float rpm, uint8_t direction)
{
	_motors[i].state = 0;
	_motors[i].time = 0;
	_motors[i].stepPin = stepPin;
	_motors[i].enablePin = enable;
	_motors[i].dirPin = dir;
	_motors[i].microStep = microStep;
	_motors[i].pulse = round(150000.0 / ( _motors[i].microStep * rpm)); //60 000 000/(200 (steps)*MicroStep*RPM*2)
	_motors[i].dir = direction;
	_motors[i].currDir = 1;
	InitPins(i);  //Call init function, to initialize pins
}

//move cycle
void DIV268N::Move(int32_t stepsFromProg[])
{
	PrepareMovement(stepsFromProg);
	StartMovement(stepsFromProg);
}

void DIV268N::PrepareMovement(int32_t stepsFromProg[])
{
	
	for (uint8_t i = 0; i < _numberOfDrivers; i++)
	{
		_stepsCount[i] += stepsFromProg[i]; //Add movement to massive of current positions
	}
	//Inverse data if needed
	for (uint8_t i = 0; i < _numberOfDrivers; i++)
	{
		if (_motors[i].dir == 1)
		{
			stepsFromProg[i] = stepsFromProg[i] * (-1); //to set another direction of move if necessary
		}
	}
	//Set direction to drivers and make all steps >0
	for (uint8_t i = 0; i < _numberOfDrivers; i++)
	{
		if (stepsFromProg[i] < 0)
		{
			_motors[i].currDir = -1; //set state of inverted work
			stepsFromProg[i] = abs(stepsFromProg[i]);  //module of steps count
			digitalWrite(_motors[i].dirPin, HIGH); //Set state of dirPin, to set direction of movement
		}
		else
		{
			_motors[i].currDir = 1; //set state of normal work
			digitalWrite(_motors[i].dirPin, LOW); //so LOW signal on direction pin
		}
	}
}

void DIV268N::StartMovement(int32_t stepsFromProg[])
{
	int32_t s = -1;
	for (uint8_t i = 0; i < _numberOfDrivers; i++)
	{
		s += stepsFromProg[i];
		stepsFromProg[i]--; //because first bit is 1, and compiler optimize code for this
		_motors[i].time = micros()+300; //set start time
	}
	uint32_t tm = 0; //timer, because micros() is too slow
	
	while (s>=0) //if steps still available - movement cycle while (CheckSteps(stepsFromProg) == 1) bitRead(*((byte*)&s + 3),7)==0
	{
		tm = micros();
		for (uint8_t i = 0; i < _numberOfDrivers; i++)
		{
			if (bitRead(*((char*)stepsFromProg+4*i+3),7) == 0 && _motors[i].time <= tm) //if his time is come if (_motors[i].Time <= micros() && _motors[i].Time != 0)
			{
				if (_motors[i].state == 0) //State 0 - motor is ready to work
				{
					ChangeStateMT(_motors[i].stepPin, 255); //set step pin high
					_motors[i].state = ~_motors[i].state; //changing state - motor in work, high cycle
					_motors[i].time += _motors[i].pulse; //add next timer alarm
					stepsFromProg[i]--; //lowering count of steps
					s--;
				}
				else //State 1, motor work, high cycle
				{
					ChangeStateMT(_motors[i].stepPin, 0); //set cycle to low
					_motors[i].state = ~_motors[i].state; //changing state of motor - motor work, low cycle
					_motors[i].time += _motors[i].pulse; //add next timer alarm
				}
			}
		}
		//*
		if (Serial.available() != 0 || Serial2.available() != 0) //if we founding data in serial port
		{ //we writing our real position
			for (uint8_t i = 0; i < _numberOfDrivers; i++)
			{
				if (_motors[i].dir == 0) //if motor no inverted
					_stepsCount[i] = _stepsCount[i] - (stepsFromProg[i] + 1) * (_motors[i].currDir); //standard, * because of direction of motor
				else
					_stepsCount[i] = _stepsCount[i] + (stepsFromProg[i] + 1) * (_motors[i].currDir); //if inverted
				stepsFromProg[i] = -1; //Movement massive =0; no need to move engines again
				s = -1;
			}
		}
		//*/
	}
	BreakMovement();
}

//Set state of Driver - on/off
void DIV268N::ChangeState(uint8_t State[])
{
	for (uint8_t i = 0; i < _numberOfDrivers; i++)
	{
		if (State[i] == 0) //Zero state - motor on
			digitalWrite(_motors[i].enablePin, LOW);
		else //1 - motor off, set high signal
			digitalWrite(_motors[i].enablePin, HIGH);
	}
}

int32_t DIV268N::ShowSteps(int16_t i)
{ //show steps of current motor from internal counter
	return(_stepsCount[i]);
}

void DIV268N::CorrectSpeed(float _RPM, int16_t i) //changing rpm of motor, need number of motor and rpm
{
	_motors[i].pulse = round(150000.0 / (_motors[i].microStep * _RPM)); //same formula as in SetMotor
}

void DIV268N::AddStep(int16_t motor, int32_t position) //Overwrite internal counter of steps
{
	_stepsCount[motor] = position; //write new data in counter
}
