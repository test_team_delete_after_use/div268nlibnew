// 
// 
// 
#include "Arduino.h"
#include "CorrectionLibrary.h"

const float z1 = 172.15;			 //Constant length from point to engine 1/3
const float z2 = 248.55;			 //Constant length from point to engine 2/4
const float xr1 = pow(5.39*1.05, 2); //List of constants, x - delta between axis and plate (horizontal) //5.39
const float xr2 = pow(2.0, 2);
const float xr3 = pow(5.39, 2);
const float xr4 = pow(17.0, 2);
const float xl1 = pow(17.0, 2);
const float xl2 = pow(5.39, 2);
const float xl3 = pow(2.0, 2);
const float xl4 = pow(5.39, 2);
const float yr1 = pow(53.84*1.05, 2); // y - delta between axis and engine (vertical) //53.83
const float yr2 = pow(33.47, 2);
const float yr3 = pow(28.21, 2);
const float yr4 = pow(52.93, 2);
const float yl1 = pow(51.9, 2);
const float yl2 = pow(25.7, 2);
const float yl3 = pow(28.2, 2);
const float yl4 = pow(56.21, 2);

const float a = pow(11,2); 
const float const2 = 31;

float dist[9]; //massive that contains converted length of thread, mm
float CorrectDist[8]; //massive that contains length of correction, mm
float *Speed = new float[9]; //massive that contains calculated speed for each engine
float *CurPos = new float[8]; //massive that contains current length of thread
float *NewPos = new float[8]; //massive that contains new length of thread

void DimasCorr(int long delta, float __diam[])  //new correction method, by Dima
{
	CurPos[0] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z1 - delta * __diam[8], 2) + xr1 + yr1)); //for each engine calculated
	CurPos[1] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z2 - delta * __diam[8], 2) + xr2 + yr2)); //current length of thread
	CurPos[2] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z1 - delta * __diam[8], 2) + xr3 + yr3)); //by Pifagor`s triangle
	CurPos[3] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z2 - delta * __diam[8], 2) + xr4 + yr4));
	CurPos[4] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z1 - delta * __diam[8], 2) + xl1 + yl1));
	CurPos[5] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z2 - delta * __diam[8], 2) + xl2 + yl2));
	CurPos[6] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z1 - delta * __diam[8], 2) + xl3 + yl3));
	CurPos[7] = -(sqrt((a + pow(const2 + delta * __diam[8], 2))) + sqrt(pow(z2 - delta * __diam[8], 2) + xl4 + yl4));
	NewPos[0] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z1 - delta * __diam[8] - dist[8], 2) + xr1 + yr1)); //same thing to a new position
	NewPos[1] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z2 - delta * __diam[8] - dist[8], 2) + xr2 + yr2)); //this calculation contains calculation
	NewPos[2] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z1 - delta * __diam[8] - dist[8], 2) + xr3 + yr3)); //of small triangle, which is the same for
	NewPos[3] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z2 - delta * __diam[8] - dist[8], 2) + xr4 + yr4)); //all engines
	NewPos[4] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z1 - delta * __diam[8] - dist[8], 2) + xl1 + yl1));
	NewPos[5] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z2 - delta * __diam[8] - dist[8], 2) + xl2 + yl2));
	NewPos[6] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z1 - delta * __diam[8] - dist[8], 2) + xl3 + yl3));
	NewPos[7] = -(sqrt((a + pow(const2 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(z2 - delta * __diam[8] - dist[8], 2) + xl4 + yl4));
	
	for (int i = 0; i < 8; i++) //so delta = difference between
	{							//new position and current position
		CorrectDist[i] = NewPos[i] - CurPos[i];
	}
}

void MoveCorr(long int _steps[], float _diam[], int long position)
{
	for (int i = 0; i < 9; i++) //converting steps to mm
	{
		dist[i] = _diam[i] * _steps[i];
	}
	
	DimasCorr(position, _diam); //call the function of correction

	for (int i = 0; i < 8; i++) //add this correction to massive, which go to move function
	{
		_steps[i] += round(CorrectDist[i] / _diam[i]); //convert mm to steps and rounding it
	}
}

float SpeedCorr(long int _steps[], int i) //speed correction: rpm=steps_of_curr_motor*const/steps_of_motor_numb8
{
	return(abs(_steps[i] * 69.5 / _steps[8])); //const is calculated by god, IDKHow
}

//Old code, old correction formula and another trash
/*
CorrectDist[0] = NewPos[0] - CurPos[0]; //so delta = difference between
	CorrectDist[1] = NewPos[1] - CurPos[1]; //new position and current position
	CorrectDist[2] = NewPos[2] - CurPos[2];
	CorrectDist[3] = NewPos[3] - CurPos[3];
	CorrectDist[4] = NewPos[4] - CurPos[4];
	CorrectDist[5] = NewPos[5] - CurPos[5];
	CorrectDist[6] = NewPos[6] - CurPos[6];
	CorrectDist[7] = NewPos[7] - CurPos[7];
	*/



/*
const float a1 = 172.15; //143.something_there, wrong data, recalculated
const float a2 = 248.55;
const float r1 = pow(61.0, 2);
const float r2 = pow(4.07, 2);
const float r3 = pow(8.87, 2);
const float r4 = pow(62.43, 2);
const float l1 = pow(51.5, 2);
const float l2 = pow(4.07, 2);
const float l3 = pow(33.3, 2);
const float l4 = pow(52.93, 2);
*/

/*const float r1 = pow(36.07, 2);
const float r2 = pow(4.07, 2);
const float r3 = pow(8.87, 2);
const float r4 = pow(52.58, 2);
const float l1 = pow(36.07, 2);
const float l2 = pow(4.07 , 2);
const float l3 = pow(33.3, 2);
const float l4 = pow(56.15, 2);
Downside
*/

//float k = -1;

//float dlini[8] = { 201.73 , 204.61};

//u=211.73

/*
CorrectDist[0] = -_steps[8] * _diam[8] + (sqrt(pow(a1 - position * _diam[8], 2) + r1) - sqrt(pow(a1 - position * _diam[8] - dist[8], 2) + r1));
CorrectDist[1] = -_steps[8] * _diam[8] + (sqrt(pow(a2 - position * _diam[8], 2) + r2) - sqrt(pow(a2 - position * _diam[8] - dist[8], 2) + r2));
CorrectDist[2] = -_steps[8] * _diam[8] + (sqrt(pow(a1 - position * _diam[8], 2) + r3) - sqrt(pow(a1 - position * _diam[8] - dist[8], 2) + r3));
CorrectDist[3] = -_steps[8] * _diam[8] + (sqrt(pow(a2 - position * _diam[8], 2) + r4) - sqrt(pow(a2 - position * _diam[8] - dist[8], 2) + r4));
CorrectDist[4] = -_steps[8] * _diam[8] + (sqrt(pow(a1 - position * _diam[8], 2) + l1) - sqrt(pow(a1 - position * _diam[8] - dist[8], 2) + l1));
CorrectDist[5] = -_steps[8] * _diam[8] + (sqrt(pow(a2 - position * _diam[8], 2) + l2) - sqrt(pow(a2 - position * _diam[8] - dist[8], 2) + l2));
CorrectDist[6] = -_steps[8] * _diam[8] + (sqrt(pow(a1 - position * _diam[8], 2) + l3) - sqrt(pow(a1 - position * _diam[8] - dist[8], 2) + l3));
CorrectDist[7] = -_steps[8] * _diam[8] + (sqrt(pow(a2 - position * _diam[8], 2) + l4) - sqrt(pow(a2 - position * _diam[8] - dist[8], 2) + l4));
	*/

	//For history
	//NewPos[7] = -(sqrt((121 + pow(31 + delta * __diam[8] + dist[8], 2))) + sqrt(pow(172.1 - delta * __diam[8] - dist[8], 2) + pow(17.5, 2) + pow(51.9, 2)));