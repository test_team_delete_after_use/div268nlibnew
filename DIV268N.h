#pragma once
#include "Arduino.h"

struct DriverParametersList// structure with all states that needed for driver
{
	uint8_t stepPin;   //number of pin of step contact...
	uint8_t dirPin;    //of dir contact...
	uint8_t enablePin; //of enable contact
	uint8_t microStep; //Set microstep of the engine, which set on Driver
	uint32_t pulse; //contains rpm, converted into pulse time
	uint32_t time; //contains time of next call, for movement cycle
	uint8_t dir; //byte, if =1 - inverted rolling, 0 - standard
	int16_t currDir; //Current state of DirPin
	uint8_t state; //State info, for movement cycle
};

class DIV268N
{
public:
	DIV268N(uint8_t count); //need to initialize library, to say how many engines we have

	void AddMotor(uint8_t numb, uint8_t enablePin, uint8_t stepPin, uint8_t dirPin, uint8_t microStep, float rpm, uint8_t direction);
	//Adding a new motor to the list, need his number,some data about driver, and direction (invers or normal)
	void Move(int32_t steps[]); //move motors to some steps, need massive of all steps for all engines (even 0)
	void ChangeState(uint8_t state[]); //Enable or disable engines, need massive for all states of each engine every time
	int32_t ShowSteps(int16_t i); //Return counter steps of engine, need his number
	void CorrectSpeed(float rpm, int16_t i); //Correct speed of engine, need his speed and number
	void AddStep(int16_t motor, int32_t position); //Correct internal counter of steps, need number of motor and new position in steps

private:
	DriverParametersList *_motors;
	int32_t *_stepsCount;
	int8_t _numberOfDrivers;
	void InitPins(int8_t i);
	void BreakMovement();
	uint8_t CheckSteps(int32_t steps[]);
	void PrepareMovement(int32_t stepsFromProg[]);
	void StartMovement(int32_t stepsFromProg[]);
};

