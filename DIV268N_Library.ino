/*
 Name:		DIV268N_Library.ino
 Created:	17.10.2018 17:01:33
 Author:	Andy
*/

#include "DIV268N.h"
#include "Arduino.h"
#include "EEPROM.h"
#include "CorrectionLibrary.h"
#include "MatLabDataParser.h"

//#define Serial Serial2
//#define Serial2 Serial

DIV268N Motor(9); //initialize library 
long int mas[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //prepare massive which we use to send to library, contains steps
byte State[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 }; //prepare massive which we use to send to library, contains state of driver
float diam[9] = { (3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8,
(3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8, (3.1415 / 3200.0)*19.8, 5.0 / 3200.0 };

String readString;  //Prepare string, which contains data from Serial Port
String convertString = ""; //Prepare string, which contains converted to old style string
//int steps;
byte interStop = 0; //flag of interrupt start
long int interrupt_count; //count of steps, which we have after going to Zero
//long int position;
byte correctionEnable = 1; //flag, which need to command back, disable calculation of correction

// read long int from EEPROM
long EEPROM_long_read(int addr) {
	byte raw[4];
	for (byte i = 0; i < 4; i++) raw[i] = EEPROM.read(addr + i);
	long &num = (long&)raw;
	return num;
}
// write long int from EEPROM
void EEPROM_long_write(int addr, long num) {
	byte raw[4];
	(long&)raw = num;
	for (byte i = 0; i < 4; i++) EEPROM.write(addr + i, raw[i]);
}


// the setup function runs once when you press reset or power the board
void setup() {
	Serial2.begin(9600);
	Serial.begin(9600); //initialize serial port
	delay(1000); //wait a little bit to establish connection
	pinMode(2, INPUT_PULLUP); //set mode of pin, which we use for interrupt
	attachInterrupt(0, interrupt1, FALLING); //initialize interrupt
	
	/*
	Motor.AddMotor(0, 5, 7, 9, 16, 70, 1); //r1
	Motor.AddMotor(1, 5, 7, 9, 16, 70, 1); //r2
	Motor.AddMotor(2, 5, 7, 9, 16, 70, 1); //r3
	Motor.AddMotor(3, 5, 7, 9, 16, 70, 1); //r4
	Motor.AddMotor(4, 5, 7, 9, 16, 70, 1); //l1
	Motor.AddMotor(5, 5, 7, 9, 16, 70, 1); //l2
	Motor.AddMotor(6, 5, 7, 9, 16, 70, 1); //l3
	Motor.AddMotor(7, 5, 7, 9, 16, 70, 1); //l4
	Motor.AddMotor(8,  5,  7,  9, 16, 970, 1);
	//*/
	//*
	Motor.AddMotor(0, 53, 51, 49, 16, 12, 0); //r1
	Motor.AddMotor(1, 52, 50, 48, 16, 12, 0); //r2
	Motor.AddMotor(2, 46, 44, 42, 16, 12, 1); //r3
	Motor.AddMotor(3, 40, 38, 36, 16, 12, 1); //r4
	Motor.AddMotor(4, 31, 29, 27, 16, 12, 1); //l1
	Motor.AddMotor(5, 25, 23, 21, 16, 12, 1); //l2
	Motor.AddMotor(6,  8,  4,  6, 16, 12, 0); //l3
	Motor.AddMotor(7, 41, 39, 37, 16, 12, 0); //l4
	Motor.AddMotor(8,  5,  7,  9, 16, 70, 1);
	//*/

	for (int i = 0; i < 9; i++) //Print data from EEPROM, to have current position
	{
		Motor.AddStep(i, EEPROM_long_read(i * 4)); //Write current position to library, need after each restart, because of correction
	}
	Serial2.println("Controller started!");
	//ShowStepsCount();
}
// the loop function runs over and over again until power down or reset
void loop() {
	while (Serial.available())
	{
		delay(3); // delay to allow buffer to fill
		if (Serial.available() > 0) // If we have data in Serial buffer
		{
			char c = Serial.read(); // gets one byte from serial buffer
			readString += c; // makes the string readString
		}
	}
	while (Serial2.available())
	{
		delay(3); // delay to allow buffer to fill
		if (Serial2.available() > 0) // If we have data in Serial buffer
		{
			char c = Serial2.read(); // gets one byte from serial buffer
			readString += c; // makes the string readString
		}
	}
	if (readString.length() > 0) //if we collect data
	{
		Serial2.print("INPUT:");
		Serial2.println(readString);
		if (readString[0] == '[')
		{
			if (readString[readString.length() - 2] == ']')
			{
				MatLabParse(readString, mas, diam); //Send this string to MathLab parser, also send massive of steps, convert coef and flag of package mode
				readString = ""; // Clear string
				for (int i = 0; i < 9; i++)
				{
					mas[i] = mas[i] - Motor.ShowSteps(i);
				}
				correctionEnable = 0;
				move(); //go to move function
				ShowStepsCount(); //show current position
			}
			else
			{
				Serial.print("T"); //send T - that shows that string is broken;
				Serial2.println("ERROR: String corrupt!!!");
				readString = "";
			}
		}
		else
		{
			convert_old_string(readString); //Convert string to old format and parse data and move
			readString = ""; //Clear string
			ShowStepsCount(); //Show current position
		}

	}
}

void ShowStepsCount() //Function which print info about position from EEPROM
{
	/*for (int i = 0; i < 9; i++) // For each engine, 9 cells, each 4 bytes
	{
		EEPROM_long_write(i * 4, Motor.ShowSteps(i));
		Serial2.print("EEPROM Data ");
		Serial2.print(i);
		Serial2.print(": ");
		Serial2.println(EEPROM_long_read(i * 4)); //print data
		mas[i] = 0;
	}
	//Serial.print("S");*/
}

void convert_old_string(String readString) //function to convert new type of string (r1 10 r2 20 ... etc), to old (a10 b20 ... etc)
{
	//convert new form to old form of command line (BYT WHY?!)
	if (readString.length() > 0)
	{
		if (readString == "Back") //if sting contains Back
		{
			for (int i = 0; i < 9; i++) //this command is returns engines to Zero position
			{
				mas[i] = -Motor.ShowSteps(i); //Simply revers current position into minus
			}
			readString = ""; //clear string
			correctionEnable = 0; //Set correction flag to zero, to avoid correction
			move();
		}
		if (readString == "Zero") //if sting contains Zero
		{
			for (int i = 0; i < 8; i++) //For radial engines (r and l) set position Zero
			{
				Motor.AddStep(i, 0); //Send new position to library
			}
			readString = ""; //Data to EEPROM would be write in move cycle
		}
		if (readString == "ZeroAll") //if sting contains Zero
		{
			for (int i = 0; i < 9; i++) //For all engines set position Zero
			{
				Motor.AddStep(i, 0); //Send new position to library
			}
			readString = ""; //Data to EEPROM would be write in move cycle
		}
		int i = 0; //Start counter of position in string
		while (i < readString.length())
		{
			if (readString[i] == 'r')
			{
				i++;
				switch (readString[i])
				{
				case '1':
					convertString += 'a';
					break;
				case '2':
					convertString += 'b';
					break;
				case '3':
					convertString += 'c';
					break;
				case '4':
					convertString += 'd';
					break;
				default:
					Serial2.println("Wrong number of Motor!");
				}
			}
			else if (readString[i] == 'l')
			{
				i++;
				switch (readString[i])
				{
				case '1':
					convertString += 'e';
					break;
				case '2':
					convertString += 'f';
					break;
				case '3':
					convertString += 'g';
					break;
				case '4':
					convertString += 'h';
					break;
				default:
					Serial2.println("Wrong number of Motor!");
				}
			}
			else if (readString[i] == 'c')
			{
				convertString += 'i';
			}
			i++;
			int j = 1;
			while (readString[i + j] != ' ' && i + j < readString.length())
				j++;
			convertString += readString.substring(i + 1, i + j) + " ";
			i += j + 1;
		}
		Serial2.println(convertString);
		if (convertString!="")
			exec(convertString);
		readString = "";
		convertString = "";
	}
}

void exec(String command)
{
	int i = 0;
	int ch = 0;
	int j = 1;
	float a = 0;
	while (i < command.length())
	{
		ch = (int)command[i];  //convert ascii code to dec, -97 is operation, to do 'a'(int)=0;
		if (command[i + 1] == '*')
		{
			State[ch - 97] = 0;
			i++;
		}
		else if (command[i + 1] == '#')
		{
			State[ch - 97] = 1;
			i++;
		}
		else if ((int)command[i + 1] == 115) //ASCII 115=s //How about change it?
		{
			j = 1;
			while (command[i + j] != ' ' && i + j < command.length())
				j++;
			Motor.CorrectSpeed(command.substring(i + 1, i + j).toFloat(), ch - 97);
		}
		else
		{
			j = 1;
			while (command[i + j] != ' ' && i + j < command.length())
				j++;
			a = command.substring(i + 1, i + j).toFloat();
			mas[ch - 97] = round(a / diam[ch - 97]);
			i += j;
		}
		i++;
	}
	move();
}

void move()
{
	if (mas[8] < 0) //if we going back
	{
		if (digitalRead(2) == 0) //Check status of interrupt pi, of low signal - button pressed, we at zero
		{
			Serial2.println("Can`t move back!!! Interrupt alert!"); //print alarm message
			mas[8] = 0; //massive zero
		}
		else //if we have high signal AND flag = 1 (we stay in zero)
		{
			if (interStop == 1)
			{
				attachInterrupt(0, interrupt1, FALLING); //We need to attach disabled interrupt
				interStop = 0; //Change flag state to normal
			}
		}
	}

	correctionEnable = 0;

	if (mas[8] != 0 && correctionEnable == 1) //If we have normal move with correction (not back command)
	{
		MoveCorr(mas, diam, Motor.ShowSteps(8)); //Send data to correction function
	}

	if (mas[8] != 0)
	{
		for (int i = 0; i < 8; i++) //Set new speeds of each driver
		{
			if (mas[i] != 0)
				Motor.CorrectSpeed(SpeedCorr(mas, i), i);
		}
	}

	Motor.Move(mas); //Function to move drivers
	for (int i = 0; i < 8; i++) //Set standard speed
		Motor.CorrectSpeed(12.0, i);
	correctionEnable = 1; //Enable correction after movement

	if (interStop == 1) {
		Serial2.print("Count interrupt: ");
		Serial2.println(interrupt_count);
		interrupt_count = 0;
	}
}

void interrupt1() //interrupt handler
{
	interrupt_count = mas[8]+1;
	for (int i = 0; i < 9; i++)
	{
		mas[i] = 0;
	}
	interStop = 1;
	detachInterrupt(0);
}